<?php
$language = array (
  'videolist' => 'Vídeos',
  'videolist:owner' => 'vídeos de %s',
  'videolist:friends' => 'Vídeos de amigos',
  'videolist:all' => 'Todos los vídeos',
  'videolist:add' => 'Añadir vídeo',
  'videolist:group' => 'Vídeos del grupo',
  'groups:enablevideolist' => 'Habilitar vídeos de grupo',
  'videolist:edit' => 'Editar este vídeo',
  'videolist:delete' => 'Eliminar este vídeo',
  'videolist:new' => 'Nuevo vídeo',
  'videolist:notification' => '%s ha añadido un nuevo vídeo:

%s
%s

Ver y comentar el nuevo vídeo:
%s',
  'videolist:delete:confirm' => 'Estas seguro de que quieres eliminar este vídeo?',
  'item:object:videolist_item' => 'Vídeo',
  'videolist:nogroup' => 'Este grupo no tiene ningún video todavía',
  'videolist:more' => 'Más videos',
  'videolist:none' => 'No hay videos colgados.',
  'river:create:object:videolist_item' => '%s ha creado el video %s',
  'river:update:object:videolist_item' => '%s ha actualizado el video %s',
  'river:comment:object:videolist_item' => '%s ha comentado en el video titulado %s',
  'videolist:title' => 'Título',
  'videolist:description' => 'Descripción',
  'videolist:video_url' => 'URL del video',
  'videolist:access_id' => 'Quien puede ver que has colgado el video?',
  'videolist:tags' => 'Añadir etiquetas',
  'videolist:error:no_save' => 'Ha habido un error guardando el video, por favor inténtalo más tarde',
  'videolist:saved' => '¡Tu video se ha guardado correctamente!',
  'videolist:deleted' => '¡Tu video se ha borrado correctamente!',
  'videolist:deletefailed' => 'Desafortunadamente este video no se puede borrar ahora. Por favor inténtalo más tarde de nuevo',
  'videolist:num_videos' => 'Número de videos a mostrar',
  'videolist:widget:description' => 'Tu lista personal de reproducción de videos.',
);
add_translation("es", $language);